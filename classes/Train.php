<?php
/**
 * Created by PhpStorm.
 * User: djordje
 * Date: 22.2.18.
 * Time: 16.17
 */

namespace classes;

/**
 * Class Train
 * @package classes
 */
class Train extends Transport
{

    /**
     * Train constructor.
     * @param $from
     * @param $to
     * @param null $number
     * @param null $seat
     * @param null $note
     */
    public function __construct($from, $to, $number = null, $seat = null, $note = null)
    {
        $this->messageTemplate  = "Take train %s from %s to %s. %s.";
        $this->from             = $from;
        $this->to               = $to;
        $this->number           = $number;
        $this->seat             = $seat;
        $this->note             = $note;
        $this->buildMessage();
    }

    /**
     * We need to have this if message is different from one in Transport class
     */
    protected function buildMessage()
    {
        $this->message = sprintf($this->messageTemplate, $this->number, $this->from, $this->to, $this->seat);
    }
}