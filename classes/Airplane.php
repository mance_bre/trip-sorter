<?php
/**
 * Created by PhpStorm.
 * User: djordje
 * Date: 22.2.18.
 * Time: 16.09
 */

namespace classes;

/**
 * Class Airplane
 * @package classes
 */
class Airplane extends Transport
{

    /**
     * Airplane constructor.
     * @param $from
     * @param $to
     * @param null $number
     * @param null $seat
     * @param null $note
     */
    public function __construct($from, $to, $number = null, $seat = null, $note = null)
    {
        $this->messageTemplate  = "From %s, take flight %s to %s. %s. %s.";
        $this->from             = $from;
        $this->to               = $to;
        $this->number           = $number;
        $this->seat             = $seat;
        $this->note             = $note;
        $this->buildMessage();
    }

    /**
     * We need to have this if message is different from one in Transport class
     */
    protected function buildMessage()
    {
        $this->message = sprintf($this->messageTemplate, $this->from, $this->number, $this->to, $this->seat, $this->note);
    }
}