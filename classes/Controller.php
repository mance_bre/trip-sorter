<?php
/**
 * Created by PhpStorm.
 * User: djordje
 * Date: 22.2.18.
 * Time: 16.26
 */

namespace classes;

/**
 * Class Controller
 * Here is where we will do all the work
 *
 * @package classes
 */
class Controller
{
    private $tickets;
    private $finalDestination;
    private $sortedTickets;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->getTicketsData();
        $this->sortDirections();
        $this->printDirections();
    }

    /**
     * @param $ticket
     */
    private function printDirectionByType($ticket)
    {
        switch ($ticket['type']) {
            case "Train":
                $Train = new Train($ticket['from'], $ticket['to'], $ticket['number'], $ticket['seat'], $ticket['note']);
                echo $Train->getMessage();
                echo "\n";
                break;
            case "Bus":
                $Train = new Bus($ticket['from'], $ticket['to'], $ticket['number'], $ticket['seat'], $ticket['note']);
                echo $Train->getMessage();
                echo "\n";
                break;
            case "Plane":
                $Train = new Airplane($ticket['from'], $ticket['to'], $ticket['number'], $ticket['seat'], $ticket['note']);
                echo $Train->getMessage();
                echo "\n";
                break;
        }
    }

    /**
     * Print directions one by one.
     */
    private function printDirections()
    {
        $rowNumber = 1;
        foreach ($this->sortedTickets as $ticket) {
            echo $rowNumber . ". ";
            $rowNumber++;
            $this->printDirectionByType($ticket);
        }

        // And print the last message.
        echo $rowNumber . ". ";
        echo "You have arrived at your final destination.\n";
    }

    /**
     * Get tickets data from json file.
     */
    private function getTicketsData()
    {
        $file = "./tickets/tickets.json";
        $string = file_get_contents($file);
        $this->tickets = json_decode($string, true);
    }

    /**
     * Sorting tickets in right order, starting from final destination to start destination.
     */
    private function sortDirections()
    {
        $this->sortedTickets = array();

        /**
         * First we found last ticket. Last ticket is ticket who's "to" does not exist in any "from", that makes it final destination.
         */
        $this->findFinalDestination();
        $lastKey = count($this->tickets) - 1; // Sorted tickets last key
        $this->sortedTickets[$lastKey] = $this->finalDestination;

        /**
         * Then we found each previous tickets starting from final ticket backwards.
         */
        $currentDestination = $this->finalDestination;
        while (count($this->sortedTickets) < count($this->tickets)) {
            $prevDestination = $this->foundPrevDestination($currentDestination);
            $lastKey = $lastKey - 1;
            $this->sortedTickets[$lastKey] = $prevDestination;
            $currentDestination = $prevDestination;
        }

        ksort($this->sortedTickets);

    }

    /**
     * @param $currentDestination
     * @return bool
     */
    private function foundPrevDestination($currentDestination)
    {
        foreach ($this->tickets as $ticket) {
            if ($ticket['to'] == $currentDestination['from']) {
                return $ticket;
            }
        }

        return false;
    }

    /**
     * Retun final destination ticket
     *
     * @return mixed
     */
    private function findFinalDestination()
    {
        foreach ($this->tickets as $ticketTo) {
            $to = $ticketTo['to'];
            $from = array();
            foreach ($this->tickets as $ticketFrom) {
                $from[] = $ticketFrom['from'];
            }

            if(!in_array($to, $from)) {
                $this->finalDestination = $ticketTo;
            }

        }

        return false;
    }

}