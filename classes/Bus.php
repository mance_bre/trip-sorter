<?php
/**
 * Created by PhpStorm.
 * User: djordje
 * Date: 22.2.18.
 * Time: 15.52
 */

namespace classes;

/**
 * Class Bus
 * @package classes
 */
class Bus extends Transport
{

    /**
     * Bus constructor.
     * @param $from
     * @param $to
     * @param null $number
     * @param null $seat
     * @param null $note
     */
    public function __construct($from, $to, $number = null, $seat = null, $note = null)
    {
        $this->messageTemplate  = "Take the airport bus from %s to %s. %s. %s";
        $this->from             = $from;
        $this->to               = $to;
        $this->number           = $number;
        $this->seat             = $seat;
        $this->note             = $note;
        $this->buildMessage();
    }
}