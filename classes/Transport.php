<?php
namespace classes;

/**
 * Base Transport
 *
 *
 * @author Djordje Mancovic <dj.mancovic@gmail.com>
 */
class Transport {

    protected $messageTemplate;
    protected $from;
    protected $to;
    protected $number;
    protected $seat;
    protected $note;
    protected $message;

    /**
     * Build message, this can be adjusted in child classes.
     */
    protected function buildMessage()
    {
        $this->message = sprintf($this->messageTemplate, $this->from, $this->to, $this->seat, $this->note);
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }
}