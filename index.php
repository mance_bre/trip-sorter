<?php
/**
 * Created by PhpStorm.
 * User: djordje
 * Date: 22.2.18.
 * Time: 15.29
 *


/* Show all errors */
ini_set('display_errors', 1);
error_reporting(-1);
date_default_timezone_set('UTC');

/**
 * @param $class
 */
function __autoload($class) {
    $path = str_replace('\\', '/', $class);
    require_once $path . '.php';
}

use classes\Controller;

/**
 * Everything is happening here.
 */
new Controller();
