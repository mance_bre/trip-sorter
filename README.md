# TRIP SORTER

## Requirements

* Linux server or virtual server.
* PHP 5.3 or above.
* That should be pretty much it.

## Instructions

* Unpack tripsorter.zip to desired location
* Run `php index.php` in root directory.
* You should see your tickets in right order.

This can be upgraded toi work with different transport types. 
Just add new class like classes (Bus, Train and Airplane) and adjust it in the way you need it.